package application;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;



public class HomeController {

	@FXML
	private Button btnSubmit;
	

	
	
	
	/*
	@FXML
	private void changepages() {
		// Handle Button event.
		btnSubmit.setOnAction((event) -> {
			
		});
  }
	*/
	
	//Method to change scenes or pages etc;
	public void changepages(ActionEvent event) throws IOException {
		Parent JobSearchParent = FXMLLoader.load(getClass().getResource("jobsearch.fxml"));
		Scene JobSearchViewScene = new Scene(JobSearchParent);
		
		//This line gets the stagine information
		Stage window = (Stage)(((Node)event.getSource()).getScene().getWindow());
		window.setScene(JobSearchViewScene);
		window.show();
	}
	
	
	
	
	
}
